package com.gdx.extension.screen.event;

import com.gdx.extension.screen.BaseScreen;
import com.gdx.extension.screen.ScreenManager;

/**
 * Used to listen to screen events.
 */
public interface ScreenListener {

    /**
     * Called when the {@link ScreenManager manager} register the screen.
     * 
     * @param screenManager the {@link ScreenManager manager} managing the screen
     * @param screen the screen that was registered
     */
    public void onRegister(ScreenManager screenManager, BaseScreen screen);

    /**
     * Called when the {@link ScreenManager manager} show the screen.
     * 
     * @param screenManager the {@link ScreenManager manager} managing the screen
     * @param screen the screen that was registered
     */
    public void onShow(ScreenManager screenManager, BaseScreen screen);

    /**
     * Called when the {@link ScreenManager manager} hide the screen.
     * 
     * @param screenManager the {@link ScreenManager manager} managing the screen
     * @param screen the screen that was registered
     */
    public void onHide(ScreenManager screenManager, BaseScreen screen);

    /**
     * Called when the {@link ScreenManager manager} register the screen.
     * 
     * @param screenManager the {@link ScreenManager manager} managing the screen
     * @param screen the screen that was registered
     */
    public void onUnregister(ScreenManager screenManager, BaseScreen screen);
}