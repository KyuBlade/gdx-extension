package com.gdx.extension.screen.event;

import com.gdx.extension.screen.BaseScreen;
import com.gdx.extension.screen.ScreenManager;

public class ScreenListenerAdapter implements ScreenListener {

    @Override
    public void onRegister(ScreenManager screenManager, BaseScreen screen) {
    }

    @Override
    public void onShow(ScreenManager screenManager, BaseScreen screen) {
    }

    @Override
    public void onHide(ScreenManager screenManager, BaseScreen screen) {
    }

    @Override
    public void onUnregister(ScreenManager screenManager, BaseScreen screen) {
    }
}