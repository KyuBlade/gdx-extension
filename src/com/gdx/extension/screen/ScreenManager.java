/** Copyright 2013
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.gdx.extension.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Stack;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.reflect.ClassReflection;
import com.gdx.extension.screen.event.ScreenListener;

/**
 * A screen manager allow to handle severals screens at a time.<br />
 * It's not only for rendering, you can use screens as AppStates.
 */
public class ScreenManager implements Disposable {

    /**
     * Propagate inputs
     */
    private InputMultiplexer inputProcessor;

    /**
     * You can access this skin in your screen
     */
    private Skin skin;

    /**
     * Catch events and draw actors
     */
    private Stage stage;

    /**
     * Overlap all screen layouts
     */
    private Stack stageStack;

    /**
     * Registered screens
     */
    private Array<BaseScreen> screens;

    /**
     * To update screens depth rendering
     */
    private boolean needUpdate;

    /**
     * Create a new screen manager.
     * 
     * @param stage provide the stage to manage with this manager
     * @param skin the skin to store in this manager for future use in your screens
     */
    public ScreenManager(Stage stage, Skin skin, InputMultiplexer parentInputProcessor) {
	if (stage == null) {
	    throw new NullPointerException("Stage must not be null.");
	} else if (parentInputProcessor == null) {
	    throw new NullPointerException("Parent input processor must not be null.");
	}

	this.stage = stage;
	this.skin = skin;

	inputProcessor = new InputMultiplexer();
	inputProcessor.addProcessor(stage);
	parentInputProcessor.addProcessor(inputProcessor);

	stageStack = new Stack();
	stage.addActor(stageStack);
	stageStack.setFillParent(true);

	screens = new Array<BaseScreen>();
    }

    /**
     * Render all active screens.
     * 
     * @param delta time from last frame
     */
    public void render(float delta) {
	if (needUpdate) {
	    screens.sort();

	    stageStack.clearChildren();
	    for (int i = 0; i < screens.size; i++) {
		BaseScreen _screen = screens.get(i);
		if (_screen.isActive) {
		    stageStack.add(_screen.layout);
		    _screen.layout.setVisible(true);
		}
	    }

	    needUpdate = false;
	}

	for (int i = 0; i < screens.size; i++) {
	    BaseScreen _screen = screens.get(i);
	    if (_screen.isActive) {
		_screen.render(delta);
	    }
	}

	stage.act(delta);
	stage.draw();
    }

    /**
     * Resize all screens.
     * 
     * @param width the new width
     * @param height the new height
     */
    public void resize(int width, int height) {
	stage.getViewport().update(width, height, true);

	for (int i = 0; i < screens.size; i++) {
	    BaseScreen _screen = screens.get(i);
	    _screen.resize(width, height);
	}
    }

    /**
     * Pause all screens.
     */
    public void pause() {
	for (int i = 0; i < screens.size; i++) {
	    BaseScreen _screen = screens.get(i);
	    _screen.pause();
	}
    }

    /**
     * Resume all screens.
     */
    public void resume() {
	for (int i = 0; i < screens.size; i++) {
	    BaseScreen _screen = screens.get(i);
	    _screen.resume();
	}
    }

    /**
     * Unregister, hide and dispose all screens.
     */
    public void dispose() {
	for (int i = 0; i < screens.size; i++) {
	    BaseScreen _screen = screens.get(i);
	    unregisterScreen(_screen);
	}

	stage.dispose();
    }

    /**
     * Add the screen to the manager and show it.
     * 
     * @param screen screen the screen to add
     * 
     * @return the added screen
     */
    public <T extends BaseScreen> T registerScreen(T screen) {
	return registerScreen(screen, true);
    }

    /**
     * Add the screen to the manager.
     * 
     * @param screen the screen to add
     * @param show true if you you wan't to show it now or false for manual showing
     * 
     * @return the added screen
     */
    public <T extends BaseScreen> T registerScreen(T screen, boolean show) {
	if (screen == null) {
	    throw new NullPointerException("Screen must not be null.");
	}

	Gdx.app.debug(
		"ScreenManager",
		"Register screen " + screen.getClass().getSimpleName() + " from thread " + Thread
			.currentThread());

	for (ScreenListener listener : screen.listeners) {
	    listener.onRegister(this, screen);
	}

	screens.add(screen);
	if (show) {
	    showScreen(screen);
	}

	return screen;
    }

    /**
     * Show the screen by rendering it.
     * 
     * @param screen the screen to show
     * @return the provided screen
     */
    public <T extends BaseScreen> T showScreen(T screen) {
	if (screen.isActive) {
	    return screen;
	}

	Gdx.app.debug(
		"ScreenManager",
		"Show screen " + screen.getClass().getSimpleName() + " from thread " + Thread
			.currentThread());

	stageStack.add(screen.layout);
	screen.layout.setVisible(false);
	screen.setActive(true);
	screen.show();

	for (ScreenListener listener : screen.listeners) {
	    listener.onShow(this, screen);
	}

	return screen;
    }

    /**
     * Show the screen by rendering it.
     * 
     * @param screen the screen to show
     * @return the provided screen or null if not registered in the manager
     */
    public <T extends BaseScreen> T showScreen(Class<T> screen) {
	T _screen = getScreen(screen);
	if (_screen == null) {
	    return null;
	}

	return showScreen(_screen);
    }

    /**
     * Hide the screen.
     * 
     * @param screen the screen to hide
     * @return the provided screen
     */
    public <T extends BaseScreen> T hideScreen(T screen) {
	if (!screen.isActive) {
	    return screen;
	}

	Gdx.app.debug(
		"ScreenManager",
		"Hide screen " + screen.getClass().getSimpleName() + " from thread " + Thread
			.currentThread());

	stageStack.removeActor(screen.layout);
	screen.setActive(false);
	screen.hide();

	for (ScreenListener listener : screen.listeners) {
	    listener.onHide(this, screen);
	}

	return screen;
    }

    /**
     * 
     * @param screen the screen to hide
     * @return the provided screen or null if not registered in the manager
     */
    public <T extends BaseScreen> T hideScreen(Class<T> screen) {
	T _screen = getScreen(screen);
	if (_screen == null) {
	    return null;
	}

	return hideScreen(_screen);
    }

    /**
     * Remove a screen from the manager and the stage and dispose it.
     * 
     * @param screen the screen type to remove
     * @return the unregistered screen or null if the screen was not in the manager
     */
    public <T extends BaseScreen> T unregisterScreen(Class<T> screen) {
	T _screen = getScreen(screen);

	return unregisterScreen(_screen);
    }

    /**
     * Remove a screen from the manager and the stage and dispose it.
     * 
     * @param screen the screen type to remove
     * @return the unregistered screen
     */
    public <T extends BaseScreen> T unregisterScreen(T screen) {
	if (screen == null) {
	    return null;
	}

	Gdx.app.debug(
		"ScreenManager",
		"Unregister screen " + screen.getClass().getSimpleName() + " from thread " + Thread
			.currentThread());

	for (ScreenListener listener : screen.listeners) {
	    listener.onUnregister(this, screen);
	}

	hideScreen(screen);
	screens.removeValue(screen, false);
	screen.dispose();

	return screen;
    }

    /**
     * Get a screen from the manager.
     * 
     * @param screen the screen type to get.
     * @return the first screen of this type or null if no one found.
     */
    @SuppressWarnings("unchecked")
    public <T extends BaseScreen> T getScreen(Class<T> screen) {
	for (int i = 0; i < screens.size; i++) {
	    BaseScreen _screen = screens.get(i);
	    if (ClassReflection.isInstance(screen, _screen)) {
		return (T) _screen;
	    }
	}

	return null;
    }

    /**
     * Force update the screens rendering order.
     */
    public void updateScreens() {
	needUpdate = true;
    }

    /**
     * @return all registered screens
     */
    public Array<BaseScreen> getScreens() {
	return screens;
    }

    /**
     * @return the stage managed by the manager
     */
    public Stage getStage() {
	return stage;
    }

    /**
     * @return the stored skin
     */
    public Skin getSkin() {
	return skin;
    }

    /**
     * @return the input processor managed by this manager
     */
    public InputMultiplexer getInputProcessor() {
	return inputProcessor;
    }
}