package com.gdx.extension.ui.input;

import java.util.Arrays;

import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Pool.Poolable;
import com.gdx.extension.ui.input.InputCatcher.Buttons;

/**
 * An array of {@link InputHolder inputs}.
 */
public class InputArray extends Array<InputHolder> implements Poolable {

    /**
     * Create an empty array of inputs.
     */
    public InputArray() {
	super(3);
    }

    /**
     * Create an array of inputs with one input.
     * 
     * @param input the input to add
     */
    public InputArray(InputHolder input) {
	super(3);

	add(input);
    }

    /**
     * Create an array with several inputs.
     * 
     * @param inputs the inputs to add
     */
    public InputArray(InputHolder... inputs) {
	super(3);

	addAll(inputs);
    }

    /**
     * Create an array of inputs and fill it from inputs literals.
     * 
     * @param literals the inputs literals to parse and add
     */
    public InputArray(String literals) {
	super(3);

	parse(literals);
    }

    /**
     * Get the literal of this input array.
     * 
     * @return the literal or null if the array is empty
     */
    @Override
    public String toString() {
	if (size <= 0) {
	    return "";
	}

	StringBuilder _sBuild = new StringBuilder();
	for (int i = 0; i < size; i++) {
	    InputHolder _input = get(i);
	    if (_input.type == InputType.Mouse) {
		_sBuild.append(Buttons.toString(_input.input));
	    } else if (_input.type == InputType.Keyboard) {
		_sBuild.append(Keys.toString(_input.input));
	    }
	    if (i + 1 < size) {
		_sBuild.append(" + ");
	    }
	}

	return _sBuild.toString();
    }

    @Override
    public int hashCode() {
	return Arrays.hashCode(items);
    }

    /**
     * Convert literals into inputs.
     * 
     * @param inputString the literal to parse
     * @throws IllegalArgumentException if unable to parse
     */
    public void parse(String inputString) {
	if (inputString == null || inputString.isEmpty()) {
	    return;
	}

	String[] _splitInputs = inputString.split(" \\+ ");
	for (int i = 0; i < _splitInputs.length; i++) {
	    String _inputString = _splitInputs[i];
	    InputHolder _input = InputHolder.fromString(_inputString);
	    if (_input == null) {
		throw new IllegalArgumentException(
			"Unable to parse the inputs string : " + inputString);
	    }
	    add(_input);
	}
    }

    /**
     * Clear this array.
     */
    @Override
    public void reset() {
	clear();
    }
}