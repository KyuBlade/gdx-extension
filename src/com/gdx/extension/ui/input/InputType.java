package com.gdx.extension.ui.input;

public enum InputType {
    Mouse, Keyboard;
}