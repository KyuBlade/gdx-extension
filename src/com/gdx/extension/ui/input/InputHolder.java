package com.gdx.extension.ui.input;

import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.utils.Pool.Poolable;
import com.gdx.extension.ui.input.InputCatcher.Buttons;

/**
 * Store one input and his type.
 */
public class InputHolder implements Poolable {

    InputType type;
    int input = -1;

    public InputHolder() {
    }

    /**
     * Create an {@link InputHolder} from a string given by {@link InputHolder#toString()}.
     * 
     * @param inputString the string to parse
     * @deprecated <br />
     * Use {@link #fromString(String)} instead
     */
    @Deprecated
    public InputHolder(String inputString) {
	if (inputString == null || inputString.isEmpty()) {
	    return;
	}

	input = Buttons.valueOf(inputString);
	if (input != -1) {
	    type = InputType.Mouse;
	    return;
	}

	input = Keys.valueOf(inputString);
	if (input != -1) {
	    type = InputType.Keyboard;
	    return;
	}

	throw new IllegalStateException("Unable to parse the input");
    }

    /**
     * Create an {@link InputHolder} from the specified input.
     * 
     * @param type the type of the input
     * @param input the input
     */
    public InputHolder(InputType type, int input) {
	this.type = type;
	this.input = input;
    }

    /**
     * Get the input type.
     * 
     * @return the type
     */
    public InputType getType() {
	return type;
    }

    /**
     * Set the type of the input.
     * 
     * @param type the type of the input
     */
    public void setType(InputType type) {
	this.type = type;
    }

    /**
     * Get the input.
     * 
     * @return the input
     */
    public int getInput() {
	return input;
    }

    /**
     * Set the input.
     * 
     * @param input the input
     */
    public void setInput(int input) {
	this.input = input;
    }

    @Override
    public void reset() {
	type = null;
	input = -1;
    }

    @Override
    public int hashCode() {
	final int _prime = 31;
	int _result = 1;
	_result = _prime * _result + type.ordinal();
	_result = _prime * _result + input;

	return _result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;

	InputHolder _other = (InputHolder) obj;
	if (type != _other.type)
	    return false;
	if (input != _other.input)
	    return false;

	return true;
    }

    /**
     * Get the input literal. Can be resolved by new InputHolder(String}.
     * 
     * @return the input literal
     */
    @Override
    public String toString() {
	if (type == InputType.Mouse) {
	    return Buttons.toString(input);
	} else if (type == InputType.Keyboard) {
	    return Keys.toString(input);
	}
	return "";
    }

    /**
     * Create an {@link InputHolder} from the specified input.
     * 
     * @param type the type of the input
     * @param input the input
     * 
     * @return the input holder
     */
    public static InputHolder fromString(String inputLiteral) {
	if (inputLiteral == null || inputLiteral.isEmpty()) {
	    return null;
	}

	int _input;
	InputType _type = null;
	_input = Buttons.valueOf(inputLiteral);
	if (_input != -1) {
	    _type = InputType.Mouse;
	} else {
	    _input = Keys.valueOf(inputLiteral);
	    if (_input != -1) {
		_type = InputType.Keyboard;
	    } else {
		return null;
	    }
	}

	return new InputHolder(_type, _input);
    }
}